<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:activiti="http://activiti.org/bpmn" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" typeLanguage="http://www.w3.org/2001/XMLSchema" expressionLanguage="http://www.w3.org/1999/XPath" targetNamespace="http://www.activiti.org/test">
  <process id="clusterStateCreationProcess" name="Cluster State Creation Process" isExecutable="true">
    <startEvent id="startevent" name="Start"></startEvent>
    <sequenceFlow id="flow1" name="start" sourceRef="startevent" targetRef="validateArgs"></sequenceFlow>
    <serviceTask id="validateArgs" name="Validate Arguements" activiti:class="com.java.taskmgr.activiti.ActivitiValidateArguments">
      <documentation>Validate if input request has valid arguments</documentation>
      <extensionElements>
        <activiti:field name="inputRequest">
          <activiti:expression>${inputRequest}</activiti:expression>
        </activiti:field>
      </extensionElements>
    </serviceTask>
    <sequenceFlow id="flow2" name="arguments validated?" sourceRef="validateArgs" targetRef="argsValidated"></sequenceFlow>
    <exclusiveGateway id="argsValidated" name="Args Validated"></exclusiveGateway>
    <sequenceFlow id="flow3" name="No, Rollback" sourceRef="argsValidated" targetRef="rollback">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${argumentsValidated == false}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="flow4" name="YES, start creating instances" sourceRef="argsValidated" targetRef="initialInfrastructureUpdate">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${argumentsValidated == true}]]></conditionExpression>
    </sequenceFlow>
    <serviceTask id="initialInfrastructureUpdate" name="Initial Infrastructure Update" activiti:class="com.java.taskmgr.activiti.ActivitiUpdateInfrastructureSnapshot">
      <documentation>Update Infrastructure Snapshot with the current state
				of cluster</documentation>
      <extensionElements>
        <activiti:field name="inputRequest">
          <activiti:expression>${inputRequest}</activiti:expression>
        </activiti:field>
        <activiti:field name="inputInfraStateEnum">
          <activiti:expression>${inputInfraStateEnum}</activiti:expression>
        </activiti:field>
      </extensionElements>
    </serviceTask>
    <sequenceFlow id="flow5" name="success?" sourceRef="initialInfrastructureUpdate" targetRef="initialUpdateSuccess"></sequenceFlow>
    <exclusiveGateway id="initialUpdateSuccess" name="initial Update Success"></exclusiveGateway>
    <sequenceFlow id="flow6" name="YES" sourceRef="initialUpdateSuccess" targetRef="provisionInstances">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${taskException == false}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="flow7" name="No, Rollback" sourceRef="initialUpdateSuccess" targetRef="rollback">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${taskException == true}]]></conditionExpression>
    </sequenceFlow>
    <serviceTask id="provisionInstances" name="Provision Instances" activiti:class="com.java.taskmgr.activiti.ActivitiProvisionInstances">
      <extensionElements>
        <activiti:field name="inputRequest">
          <activiti:expression>${inputRequest}</activiti:expression>
        </activiti:field>
      </extensionElements>
    </serviceTask>
    <sequenceFlow id="flow8" name="success?" sourceRef="provisionInstances" targetRef="provisionInstancesSuccess"></sequenceFlow>
    <exclusiveGateway id="provisionInstancesSuccess" name="provision Instances Success"></exclusiveGateway>
    <sequenceFlow id="flow9" name="YES, does customer info exist?" sourceRef="provisionInstancesSuccess" targetRef="isCustomerRegistered">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${taskException == false}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="flow10" name="No, Rollback" sourceRef="provisionInstancesSuccess" targetRef="rollback">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${taskException == true}]]></conditionExpression>
    </sequenceFlow>
    <exclusiveGateway id="isCustomerRegistered" name="is Customer Registered"></exclusiveGateway>
    <sequenceFlow id="flow11" name="YES, persist Customer Info" sourceRef="isCustomerRegistered" targetRef="persistCust">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${inputRequest.customerId != null && inputRequest.secret != null}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="flow12" name="No" sourceRef="isCustomerRegistered" targetRef="registerCust">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${inputRequest.customerId == null || inputRequest.secret == null}]]></conditionExpression>
    </sequenceFlow>
    <serviceTask id="registerCust" name="Register New Customer" activiti:class="com.java.taskmgr.activiti.ActivitiCustomerRegistration">
      <documentation>Creating customer registration</documentation>
      <extensionElements>
        <activiti:field name="inputRequest">
          <activiti:expression>${inputRequest}</activiti:expression>
        </activiti:field>
      </extensionElements>
    </serviceTask>
    <sequenceFlow id="flow13" name="success?" sourceRef="registerCust" targetRef="isCustRegSuccess"></sequenceFlow>
    <exclusiveGateway id="isCustRegSuccess" name="is registering customer successful"></exclusiveGateway>
    <sequenceFlow id="flow14" name="YES, Persist Customer Info" sourceRef="isCustRegSuccess" targetRef="persistCust">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${taskException == false}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="flow15" name="No, Rollback" sourceRef="isCustRegSuccess" targetRef="rollback">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${taskException == true}]]></conditionExpression>
    </sequenceFlow>
    <serviceTask id="persistCust" name="Persist Customer Info" activiti:class="com.java.taskmgr.activiti.ActivitiPersistCustomer">
      <documentation>Persist Infrastructure Info for the customer</documentation>
      <extensionElements>
        <activiti:field name="inputRequest">
          <activiti:expression>${inputRequest}</activiti:expression>
        </activiti:field>
      </extensionElements>
    </serviceTask>
    <sequenceFlow id="flow16" name="success?" sourceRef="persistCust" targetRef="persistCustomerSuccess"></sequenceFlow>
    <exclusiveGateway id="persistCustomerSuccess" name="persist Customer Success"></exclusiveGateway>
    <sequenceFlow id="flow17" name="YES" sourceRef="persistCustomerSuccess" targetRef="puppetOrchestrator">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${taskException == false}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="flow18" name="No, Rollback" sourceRef="persistCustomerSuccess" targetRef="rollback">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${taskException == true}]]></conditionExpression>
    </sequenceFlow>
    <serviceTask id="puppetOrchestrator" name="Initiate Puppet Orchestrator" activiti:class="com.java.taskmgr.activiti.ActivitiPuppetOrchestrate">
      <extensionElements>
        <activiti:field name="inputRequest">
          <activiti:expression>${inputRequest}</activiti:expression>
        </activiti:field>
        <activiti:field name="waitForDns">
          <activiti:expression>${waitForDns}</activiti:expression>
        </activiti:field>
      </extensionElements>
    </serviceTask>
    <sequenceFlow id="flow19" name="success?" sourceRef="puppetOrchestrator" targetRef="puppetOrchSuccess"></sequenceFlow>
    <exclusiveGateway id="puppetOrchSuccess" name="puppet Orch Success"></exclusiveGateway>
    <sequenceFlow id="flow20" name="YES" sourceRef="puppetOrchSuccess" targetRef="updateInfrastructureSnapshot">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${taskException == false}]]></conditionExpression>
    </sequenceFlow>
    <sequenceFlow id="flow21" name="No, Rollback" sourceRef="puppetOrchSuccess" targetRef="rollback">
      <conditionExpression xsi:type="tFormalExpression"><![CDATA[${taskException == true}]]></conditionExpression>
    </sequenceFlow>
    <serviceTask id="updateInfrastructureSnapshot" name="Update Infrastructure Snapshot" activiti:class="com.java.taskmgr.activiti.ActivitiUpdateInfrastructureSnapshot">
      <documentation>Update Infrastructure Snapshot with the current state
				of cluster</documentation>
      <extensionElements>
        <activiti:field name="inputRequest">
          <activiti:expression>${inputRequest}</activiti:expression>
        </activiti:field>
        <activiti:field name="inputInfraStateEnum">
          <activiti:expression>${inputInfraStateEnum}</activiti:expression>
        </activiti:field>
      </extensionElements>
    </serviceTask>
    <sequenceFlow id="flow22" name="end" sourceRef="updateInfrastructureSnapshot" targetRef="endevent"></sequenceFlow>
    <endEvent id="endevent" name="End"></endEvent>
    <serviceTask id="rollback" name="Rollback" activiti:class="com.java.taskmgr.activiti.ActivitiRollbackImpl"></serviceTask>
    <sequenceFlow id="flow23" sourceRef="rollback" targetRef="endevent"></sequenceFlow>
  </process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_clusterStateCreationProcess">
    <bpmndi:BPMNPlane bpmnElement="clusterStateCreationProcess" id="BPMNPlane_clusterStateCreationProcess">
      <bpmndi:BPMNShape bpmnElement="startevent" id="BPMNShape_startevent">
        <omgdc:Bounds height="35.0" width="35.0" x="10.0" y="51.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="validateArgs" id="BPMNShape_validateArgs">
        <omgdc:Bounds height="55.0" width="130.0" x="162.0" y="41.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="argsValidated" id="BPMNShape_argsValidated">
        <omgdc:Bounds height="40.0" width="40.0" x="390.0" y="48.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="initialInfrastructureUpdate" id="BPMNShape_initialInfrastructureUpdate">
        <omgdc:Bounds height="61.0" width="130.0" x="346.0" y="140.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="initialUpdateSuccess" id="BPMNShape_initialUpdateSuccess">
        <omgdc:Bounds height="40.0" width="40.0" x="390.0" y="245.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="provisionInstances" id="BPMNShape_provisionInstances">
        <omgdc:Bounds height="55.0" width="127.0" x="163.0" y="238.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="provisionInstancesSuccess" id="BPMNShape_provisionInstancesSuccess">
        <omgdc:Bounds height="40.0" width="40.0" x="206.0" y="357.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="isCustomerRegistered" id="BPMNShape_isCustomerRegistered">
        <omgdc:Bounds height="40.0" width="40.0" x="67.0" y="470.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="registerCust" id="BPMNShape_registerCust">
        <omgdc:Bounds height="61.0" width="151.0" x="190.0" y="460.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="isCustRegSuccess" id="BPMNShape_isCustRegSuccess">
        <omgdc:Bounds height="40.0" width="40.0" x="399.0" y="470.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="persistCust" id="BPMNShape_persistCust">
        <omgdc:Bounds height="55.0" width="151.0" x="181.0" y="591.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="persistCustomerSuccess" id="BPMNShape_persistCustomerSuccess">
        <omgdc:Bounds height="40.0" width="40.0" x="236.0" y="683.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="puppetOrchestrator" id="BPMNShape_puppetOrchestrator">
        <omgdc:Bounds height="65.0" width="128.0" x="193.0" y="780.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="puppetOrchSuccess" id="BPMNShape_puppetOrchSuccess">
        <omgdc:Bounds height="40.0" width="40.0" x="399.0" y="792.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="updateInfrastructureSnapshot" id="BPMNShape_updateInfrastructureSnapshot">
        <omgdc:Bounds height="58.0" width="148.0" x="530.0" y="784.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="endevent" id="BPMNShape_endevent">
        <omgdc:Bounds height="35.0" width="35.0" x="930.0" y="360.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="rollback" id="BPMNShape_rollback">
        <omgdc:Bounds height="55.0" width="105.0" x="760.0" y="350.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge bpmnElement="flow1" id="BPMNEdge_flow1">
        <omgdi:waypoint x="45.0" y="68.0"></omgdi:waypoint>
        <omgdi:waypoint x="162.0" y="68.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="22.0" x="16.0" y="30.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow2" id="BPMNEdge_flow2">
        <omgdi:waypoint x="292.0" y="68.0"></omgdi:waypoint>
        <omgdi:waypoint x="390.0" y="68.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="36.0" width="100.0" x="360.0" y="13.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow3" id="BPMNEdge_flow3">
        <omgdi:waypoint x="430.0" y="68.0"></omgdi:waypoint>
        <omgdi:waypoint x="758.0" y="68.0"></omgdi:waypoint>
        <omgdi:waypoint x="812.0" y="350.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="62.0" x="557.0" y="51.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow4" id="BPMNEdge_flow4">
        <omgdi:waypoint x="410.0" y="88.0"></omgdi:waypoint>
        <omgdi:waypoint x="411.0" y="140.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="36.0" width="100.0" x="412.0" y="94.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow5" id="BPMNEdge_flow5">
        <omgdi:waypoint x="411.0" y="201.0"></omgdi:waypoint>
        <omgdi:waypoint x="410.0" y="245.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="48.0" x="391.0" y="291.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow6" id="BPMNEdge_flow6">
        <omgdi:waypoint x="390.0" y="265.0"></omgdi:waypoint>
        <omgdi:waypoint x="290.0" y="265.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="21.0" x="332.0" y="245.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow7" id="BPMNEdge_flow7">
        <omgdi:waypoint x="430.0" y="265.0"></omgdi:waypoint>
        <omgdi:waypoint x="758.0" y="265.0"></omgdi:waypoint>
        <omgdi:waypoint x="812.0" y="350.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="62.0" x="459.0" y="245.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow8" id="BPMNEdge_flow8">
        <omgdi:waypoint x="226.0" y="293.0"></omgdi:waypoint>
        <omgdi:waypoint x="226.0" y="357.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="48.0" x="236.0" y="345.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow9" id="BPMNEdge_flow9">
        <omgdi:waypoint x="206.0" y="377.0"></omgdi:waypoint>
        <omgdi:waypoint x="87.0" y="378.0"></omgdi:waypoint>
        <omgdi:waypoint x="87.0" y="470.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="36.0" width="100.0" x="95.0" y="395.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow10" id="BPMNEdge_flow10">
        <omgdi:waypoint x="246.0" y="377.0"></omgdi:waypoint>
        <omgdi:waypoint x="760.0" y="377.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="62.0" x="300.0" y="357.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow11" id="BPMNEdge_flow11">
        <omgdi:waypoint x="87.0" y="510.0"></omgdi:waypoint>
        <omgdi:waypoint x="87.0" y="619.0"></omgdi:waypoint>
        <omgdi:waypoint x="181.0" y="618.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="36.0" width="100.0" x="94.0" y="537.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow12" id="BPMNEdge_flow12">
        <omgdi:waypoint x="107.0" y="490.0"></omgdi:waypoint>
        <omgdi:waypoint x="190.0" y="490.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="14.0" x="119.0" y="499.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow13" id="BPMNEdge_flow13">
        <omgdi:waypoint x="341.0" y="490.0"></omgdi:waypoint>
        <omgdi:waypoint x="399.0" y="490.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="48.0" x="389.0" y="449.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow14" id="BPMNEdge_flow14">
        <omgdi:waypoint x="419.0" y="510.0"></omgdi:waypoint>
        <omgdi:waypoint x="419.0" y="618.0"></omgdi:waypoint>
        <omgdi:waypoint x="332.0" y="618.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="36.0" width="100.0" x="420.0" y="555.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow15" id="BPMNEdge_flow15">
        <omgdi:waypoint x="439.0" y="490.0"></omgdi:waypoint>
        <omgdi:waypoint x="758.0" y="490.0"></omgdi:waypoint>
        <omgdi:waypoint x="812.0" y="405.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="62.0" x="459.0" y="499.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow16" id="BPMNEdge_flow16">
        <omgdi:waypoint x="256.0" y="646.0"></omgdi:waypoint>
        <omgdi:waypoint x="256.0" y="683.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="48.0" x="262.0" y="659.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow17" id="BPMNEdge_flow17">
        <omgdi:waypoint x="256.0" y="723.0"></omgdi:waypoint>
        <omgdi:waypoint x="257.0" y="780.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="21.0" x="262.0" y="749.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow18" id="BPMNEdge_flow18">
        <omgdi:waypoint x="276.0" y="703.0"></omgdi:waypoint>
        <omgdi:waypoint x="758.0" y="702.0"></omgdi:waypoint>
        <omgdi:waypoint x="812.0" y="405.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="62.0" x="299.0" y="713.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow19" id="BPMNEdge_flow19">
        <omgdi:waypoint x="321.0" y="812.0"></omgdi:waypoint>
        <omgdi:waypoint x="399.0" y="812.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="48.0" x="327.0" y="826.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow20" id="BPMNEdge_flow20">
        <omgdi:waypoint x="439.0" y="812.0"></omgdi:waypoint>
        <omgdi:waypoint x="530.0" y="813.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="21.0" x="451.0" y="821.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow21" id="BPMNEdge_flow21">
        <omgdi:waypoint x="419.0" y="832.0"></omgdi:waypoint>
        <omgdi:waypoint x="418.0" y="920.0"></omgdi:waypoint>
        <omgdi:waypoint x="758.0" y="920.0"></omgdi:waypoint>
        <omgdi:waypoint x="812.0" y="405.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="62.0" x="425.0" y="871.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow22" id="BPMNEdge_flow22">
        <omgdi:waypoint x="678.0" y="813.0"></omgdi:waypoint>
        <omgdi:waypoint x="758.0" y="813.0"></omgdi:waypoint>
        <omgdi:waypoint x="947.0" y="395.0"></omgdi:waypoint>
        <bpmndi:BPMNLabel>
          <omgdc:Bounds height="12.0" width="18.0" x="939.0" y="370.0"></omgdc:Bounds>
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="flow23" id="BPMNEdge_flow23">
        <omgdi:waypoint x="865.0" y="377.0"></omgdi:waypoint>
        <omgdi:waypoint x="930.0" y="377.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</definitions>