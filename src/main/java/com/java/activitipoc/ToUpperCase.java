package com.java.activitipoc;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class ToUpperCase implements JavaDelegate {

	private String input = "hello world";
	
	public void execute(DelegateExecution execution) throws Exception {
		System.out.println("Reached the service class");
		String var = input;
		var = var.toUpperCase();
		execution.setVariable("input", var);
		System.out.println(execution.getVariable("input"));
	}

}