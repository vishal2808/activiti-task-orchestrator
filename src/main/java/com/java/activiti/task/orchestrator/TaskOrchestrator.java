package com.java.activiti.task.orchestrator;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.ProcessInstance;

import com.java.taskmgr.beans.InfraRequest;
import com.java.taskmgr.beans.InfrastructureStateEnum;

public class TaskOrchestrator {

    public static void main(String[] args) {
        InfraRequest request = new InfraRequest();
        //request.setCustomerId("urn:ebay-marketplace-consumerid:0002aab1-39be-4f98-9290-e7dfb442c4e6");
        request.setCustomerId(null);
        request.setSecret("kuchsecret");
        request.setName("testproject112");
        request.setIngestionSupport(true);
        request.setSecure(true);
        request.setIsactive(true);
        request.setCloudProject("testCloud");

        TaskOrchestrator orchestrator = new TaskOrchestrator();
        Map<String, Object> variableMap = new HashMap<>();
        variableMap.put("inputRequest", request);
        variableMap.put("inputInfraStateEnum", InfrastructureStateEnum.INFRA_CREATESNAPSHOT);
        variableMap.put("waitForDns", false);
        variableMap.put("argumentsValidated", false);
        variableMap.put("taskException", false);
        orchestrator.initiateClusterStateCreationProcess(variableMap);
    }

    public void initiateClusterStateCreationProcess(
            Map<String, Object> variableMap) {
        // Create Activiti process engine
        ProcessEngine processEngine = ProcessEngineConfiguration
                .createStandaloneInMemProcessEngineConfiguration()
                // .setJdbcDriver("com.mysql.jdbc.Driver")
                // .setDatabaseType("mysql")
                // .setJdbcUrl("jdbc:mysql://localhost:3306/activiti")
                // .setJdbcUsername("root").setJdbcPassword("")
                .setDatabaseSchemaUpdate(
                        ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE)
                .buildProcessEngine();

        // Get Activiti services
        RepositoryService repositoryService = processEngine
                .getRepositoryService();
        RuntimeService runtimeService = processEngine.getRuntimeService();

        // Deploy the process definition
        repositoryService.createDeployment()
                .addClasspathResource("ClusterStateCreationProcess.bpmn")
                .deploy();

        // Start a process instance
        ProcessInstance processInstance = runtimeService
                .startProcessInstanceByKey("clusterStateCreationProcess",
                        variableMap);
        String procId = processInstance.getId();

        // verify that the process is actually finished
        HistoryService historyService = processEngine.getHistoryService();
        HistoricProcessInstance historicProcessInstance = historyService
                .createHistoricProcessInstanceQuery().processInstanceId(procId)
                .singleResult();
        System.out.println("Process instance end time: "
                + historicProcessInstance.getEndTime());
    }
}
