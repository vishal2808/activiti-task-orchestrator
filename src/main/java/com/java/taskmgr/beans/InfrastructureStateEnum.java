package com.java.taskmgr.beans;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum InfrastructureStateEnum {

	INFRA_CREATESNAPSHOT("CREATE_SNAPSHOT", "Provsioning of machines started"), 
	INFRA_REGISTER_CUSTOMER("REGISTER_CUSTOMER", "Create customer"), 
	INFRA_CREATE_CUSTOMER_DETAILS("CREATE_CUSTOMER_DETAILS", "Store customer details to DB"),
	INFRA_PROVISION_MACHINES("ACQUIRE_MACHINES", "Nova provisioning intititated"), 
	INFRA_CONFIGURE_MACHINES("CONFIGURE_MACHINES", "Consifure all machines"), 
	INFRA_SUCCESSFUL("SUCCESS", "Infrastructure provisioned"),
	INFRA_ROLLBACK("ROLLBACK", "Rollback Infrastructure"),
	INFRA_INVALID("INVALID", "INVALID Infrastructure"),
	INFRA_FAILED("FAILED","Failed infrastructure");

	private String state;
	private String message;

	InfrastructureStateEnum(String state, String message) {
		this.state = state;
		this.message = message;
	}

	public String getState() {
		return this.state;
	}

	public String getMessage() {
		return this.message;
	}

	private static Map<InfrastructureStateEnum, String> InfraMapper = new HashMap<InfrastructureStateEnum, String>();

	static {
		List<InfrastructureStateEnum> enumList = Arrays.asList(InfrastructureStateEnum
				.values());
		for (InfrastructureStateEnum state : enumList) {
			InfraMapper.put(state, state.getState());	
		}

	}

	public static String getMap(InfrastructureStateEnum enumVal) {
		return InfraMapper.get(enumVal);
	}

}
