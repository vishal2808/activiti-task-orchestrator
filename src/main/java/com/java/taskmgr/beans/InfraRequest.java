package com.java.taskmgr.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

public class InfraRequest implements Serializable {

    private static final long serialVersionUID = 3436750190336588500L;
    @JsonProperty("name")
    private String name;
    @JsonProperty("ingestionSupport")
    private Boolean ingestionSupport;
    @JsonProperty("secure")
    private Boolean secure;
    @JsonProperty("cloudProject")
    private String cloudProject;
    @JsonProperty("customerId")
    private String customerId;
    @JsonProperty("secret")
    private String secret;
    @JsonProperty("isactive")
    private Boolean isactive;
    @JsonProperty("rollback")
    private Boolean rollback;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("customerId")
    public String getCustomerId() {
        return customerId;
    }

    @JsonProperty("customerId")
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @JsonProperty("secret")
    public String getSecret() {
        return secret;
    }

    @JsonProperty("secret")
    public void setSecret(String secret) {
        this.secret = secret;
    }

    /**
     *
     * @return The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return The ingestionSupport
     */
    @JsonProperty("ingestionSupport")
    public Boolean getIngestionSupport() {
        return ingestionSupport;
    }

    /**
     *
     * @param ingestionSupport The ingestionSupport
     */
    @JsonProperty("ingestionSupport")
    public void setIngestionSupport(Boolean ingestionSupport) {
        this.ingestionSupport = ingestionSupport;
    }

    /**
     *
     * @return The secure
     */
    @JsonProperty("secure")
    public Boolean getSecure() {
        return secure;
    }

    /**
     *
     * @param secure The secure
     */
    @JsonProperty("secure")
    public void setSecure(Boolean secure) {
        this.secure = secure;
    }

    /**
     *
     * @return The cloudProject
     */
    @JsonProperty("cloudProject")
    public String getCloudProject() {
        return cloudProject;
    }

    /**
     *
     * @param cloudProject The cloudProject
     */
    @JsonProperty("cloudProject")
    public void setCloudProject(String cloudProject) {
        this.cloudProject = cloudProject;
    }

    /**
     *
     * @return The isactive
     */
    @JsonProperty("isactive")
    public Boolean getIsactive() {
        return isactive;
    }

    /**
     *
     * @param isactive The isactive
     */
    @JsonProperty("isactive")
    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    /**
     *
     * @return The rollback
     */
    @JsonProperty("rollback")
    public Boolean getRollback() {
        return rollback;
    }

    /**
     *
     * @param rollback The rollback
     */
    @JsonProperty("rollback")
    public void setRollback(Boolean rollback) {
        this.rollback = rollback;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String toString() {
        return "InfrastructureRequest [name=" + name + ", ingestionSupport="
                + ingestionSupport + ", secure=" + secure + ", cloudProject="
                + cloudProject + ", customerId=" + customerId + ", secret="
                + secret + ", isactive=" + isactive + ", rollback=" + rollback 
                + ", additionalProperties=" + additionalProperties + "]";
    }
}
