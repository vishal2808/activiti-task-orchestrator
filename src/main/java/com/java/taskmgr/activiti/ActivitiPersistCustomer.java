package com.java.taskmgr.activiti;

import com.java.taskmgr.beans.InfraRequest;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.log4j.Logger;

public class ActivitiPersistCustomer implements JavaDelegate {

    static Logger log = Logger.getLogger(ActivitiPersistCustomer.class);
    private Expression inputRequest;
    private Expression inputInfraStateEnum;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
	InfraRequest request = (InfraRequest) inputRequest
		.getValue(execution);
	log.debug("ActivitiPersistCustomer working on the request :: "
		+ request);
	try {
	    log.info("CustomerId and Secret alreday set in request as "
		    + request.getCustomerId() + " and " + request.getSecret());
	    log.info("Skipping Obtaining Infrastructure step ...");
	    System.out.println(request.getName());
	    log.info("Persisting customer info successful");
	    execution.setVariable("inputInfraStateEnum", "INFRA_SUCCESSFUL");
	} catch (Exception e) {
	    log.error("Persisting customer info failed :: " + e.getMessage());
	    execution.setVariable("inputInfraStateEnum", "INFRA_FAILED");
	}
    }
}
