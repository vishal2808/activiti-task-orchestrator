package com.java.taskmgr.activiti;

import com.java.taskmgr.beans.InfraRequest;
import java.util.logging.Level;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import java.util.logging.Logger;
import javax.management.InvalidAttributeValueException;

public class ActivitiCustomerRegistration implements JavaDelegate {

    static final Logger log = Logger.getLogger(ActivitiCustomerRegistration.class.toString());
    private Expression inputRequest;
    private Expression inputInfraStateEnum;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
	InfraRequest request = (InfraRequest) inputRequest
		.getValue(execution);
	execution.setVariable("custRegException", false);   
	log.log(Level.INFO, "ActivitiCustomerRegistration working on the request :: {0}", request);
	try {
	    // Create customer in AR
	    if (request.getName() != null) {
		log.info("Customer already registered in backend");
		throw new InvalidAttributeValueException(
			"Customer already registered in backend");
	    } else {
		log.info("Creating customer registration");
		request.setName("New Customer");
	    }
	} catch (InvalidAttributeValueException e) {
	    log.log(Level.SEVERE, "customer registration failed :: {0}",  e.getMessage());
	    execution.setVariable("inputInfraStateEnum", "INFRA_FAILED");
	    execution.setVariable("custRegException", true);
	} catch (Exception e) {
	    log.log(Level.SEVERE, "User Already exists :: {0}", e.getMessage());
	}
    }
}
