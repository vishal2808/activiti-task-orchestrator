package com.java.taskmgr.activiti;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class ActivitiRollbackImpl implements JavaDelegate {

	@Override
	public void execute(DelegateExecution arg0) throws Exception {
		System.out.println("Cluster creation couldn't be completed");
		System.out.println("Rolling back ... ");
		System.out.println("Rollback complete");
	}
	
}
