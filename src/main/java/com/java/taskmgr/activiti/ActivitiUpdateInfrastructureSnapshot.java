package com.java.taskmgr.activiti;

import com.java.taskmgr.beans.InfraRequest;
import com.java.taskmgr.beans.InfrastructureStateEnum;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.log4j.Logger;

public class ActivitiUpdateInfrastructureSnapshot implements JavaDelegate {

    static Logger log = Logger.getLogger(ActivitiCustomerRegistration.class);
    private Expression inputRequest;
    private Expression inputInfraStateEnum;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
	InfraRequest request = (InfraRequest) inputRequest
		.getValue(execution);
	Object infraStateEnum = inputInfraStateEnum.getValue(execution);
//	log.debug("ActivitiUpdateInfrastructureSnapshot working on the request :: "
//		+ request + " and infraStateEnum " + infraStateEnum);

	System.out.println(inputInfraStateEnum.getValue(execution));
	execution.setVariable("inputInfraStateEnum", infraStateEnum);
	log.info("InfrastructureSnapshot updated with " + infraStateEnum);
    }

}
