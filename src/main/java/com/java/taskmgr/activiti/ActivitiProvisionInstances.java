package com.java.taskmgr.activiti;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.log4j.Logger;

import com.java.taskmgr.beans.InfraRequest;

public class ActivitiProvisionInstances implements JavaDelegate {

	static Logger log = Logger.getLogger(ActivitiValidateArguments.class);
	private Expression inputRequest;

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		InfraRequest request = (InfraRequest) inputRequest
				.getValue(execution);
		execution.setVariable("waitForDns", false);
		try {
			if (request.getAdditionalProperties() == null) {
				String provisioner = "NewProvisioning";
				System.out.println(provisioner);
				execution.setVariable("waitForDns", true);
			}
		} catch (Exception ex) {
			log.error("Exception occured while provisioning instances :: "
					+ ex.getMessage());
			execution.setVariable("taskException", true);
		}
	}
}
