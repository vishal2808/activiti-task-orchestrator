package com.java.taskmgr.activiti;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.log4j.Logger;

import com.java.taskmgr.beans.InfraRequest;

public class ActivitiValidateArguments implements JavaDelegate {

    static Logger log = Logger.getLogger(ActivitiValidateArguments.class);
    private Expression inputRequest;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
	InfraRequest request = (InfraRequest) inputRequest
		.getValue(execution);

	if (request == null) {
	    log.error("Request passed is null");
	    log.error("Invalid arguments passed to function on createInstance method");
	    return;
	}

	if (request.getName() == null || request.getCloudProject() == null) {
	    log.error("Invalid arguments passed to function");
	    return;
	}

	execution.setVariable("argumentsValidated", true);
    }
}
