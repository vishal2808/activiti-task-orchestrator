package com.java.taskmgr.activiti;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.log4j.Logger;

import com.java.taskmgr.beans.InfraRequest;

public class ActivitiPuppetOrchestrate implements JavaDelegate {

	static Logger log = Logger.getLogger(ActivitiValidateArguments.class);
	private Expression inputRequest;
	private Expression waitForDns;

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		InfraRequest request = (InfraRequest) inputRequest
				.getValue(execution);
		Boolean wait = (Boolean) waitForDns.getValue(execution);

		// TODO: Remove this hack
		if (wait == true) {
			try {
				log.info("Sleeping to ensure FQDN syncs ::" + request.getName());
				//Thread.sleep(900 * 1000);
				Thread.sleep(1000);
				log.info("Sleeping over ::" + request.getName());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		try {
			// Perform Orchestration of machines
			String orchestrator = "Orchestrator()";
			System.out.println(orchestrator);
			//No puppet modules specified during init
		} catch (Exception ex) {
			log.error("Exception occured while updating infrascructure :: "
					+ ex.getMessage());
			execution.setVariable("taskException", true);
		}
	}
}
