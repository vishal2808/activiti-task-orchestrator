package com.java.taskmgr.activiti;

import com.java.taskmgr.beans.InfraRequest;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiTestCase;
import org.activiti.engine.test.Deployment;
import org.junit.Test;

public class TaskOrchestratorTest extends ActivitiTestCase{

    @Test
    @Deployment(resources = { "ClusterStateCreationProcess.bpmn" })
    public void test() {
	RepositoryService repositoryService = this.repositoryService;
	repositoryService.createDeployment()
		.addClasspathResource("ClusterStateCreationProcess.bpmn")
		.deploy();
	RuntimeService runtimeService = this.runtimeService;

	InfraRequest request = new InfraRequest();
	request.setCustomerId("urn:ebay-marketplace-consumerid:0002aab1-39be-4f98-9290-e7dfb442c4e6");
	request.setSecret("kuchsecret");
	request.setName("kuchname1");

	Map<String, Object> variableMap = new HashMap<>();
	variableMap.put("inputRequest", request);
	variableMap.put("inputInfraStateEnum", "INFRA_CREATESNAPSHOT");
	ProcessInstance processInstance1 = runtimeService.startProcessInstanceByKey("clusterStateCreationProcess", variableMap);
	assertNotNull(processInstance1);
	
	request.setCustomerId(null);
	variableMap.put("inputRequest", request);
	ProcessInstance processInstance2 = runtimeService.startProcessInstanceByKey("clusterStateCreationProcess", variableMap);
	assertNotNull(processInstance2);

	Task task = this.taskService.createTaskQuery()
		.singleResult();
	//assertEquals("Activiti is awesome!", task.getName());
    }

}