# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a task orchestrator built using Activiti. Managing a taskflow works like a charm using activiti
* 1.0.0

### How do I get set up? ###

* Open the project with Eclipse and have Activiti BPMN plugin installed. (http://docs.alfresco.com/4.1/tasks/wf-install-activiti-designer.html)

### Who do I talk to? ###

* Vishal Sharma (vishal.2808@gmail.com)